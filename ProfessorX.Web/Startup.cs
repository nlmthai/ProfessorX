﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProfessorX.Web.Startup))]
namespace ProfessorX.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
