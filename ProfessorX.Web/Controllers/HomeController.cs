﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProfessorX.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string env = ConfigurationManager.AppSettings["Environment"].ToString();

            if (env == "dev")
            {
                return View("About");
            }
            else if (env == "staging")
            {
                return View("Contact");
            }
            else
            {
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}